# Airbnb data visualisations

## Visualisation codes in Jupyter Notebook


- Airbn_figures_p: script for figures in Gyódi, K. (2021). Airbnb and hotels during COVID-19: different strategies to survive. International Journal of Culture, Tourism and Hospitality Research.

- Maps_p: script to create maps as in : Gyódi, K., & Nawaro, Ł. (2021). Determinants of Airbnb prices in European cities: A spatial econometrics approach. Tourism Management, 86, 104319.

- uni_chart_set_p: script to summarise regression results in attractive graph as in: Gyódi, K., & Nawaro, Ł. (2021). Determinants of Airbnb prices in European cities: A spatial econometrics approach. Tourism Management, 86, 104319.
